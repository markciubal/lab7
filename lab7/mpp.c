#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void uncomment(char *s);
int include_file(char *s, FILE *write_file);
void write_to_file(char *s, FILE *write_file);

int main(int argc, char *argv[])
{
    // Check to see if user supplied 2 arguments
    if (argc < 2)
    {
        printf("Please supply an input and optionally an output file\n");
        exit(2);
    }
    
    // Open first argument for reading
    FILE *fp;
    fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    // Open second argument for writing
    FILE *fpout;
    if (argc == 3)
    {
        
        fpout = fopen(argv[2], "w");
        if (!fpout)
        {
            printf("Can't open %s for writing\n", argv[2]);
            exit(1);
        }
    }
    
    // Read all lines and print them as we go
    char line[100];
    while(fgets(line, 100, fp) != NULL)
    {
        uncomment(line);
        if (include_file(line, fpout) != 0)
        {
             printf("%s", line);
        }
        else
        {
            printf("%c", '\n');
        }
        if (argc == 3)
        {
            write_to_file(line, fpout);
            //printf("%s", argv[2]);
        }
        else
        {
            printf("%s", line);
        }
    }
    printf("\n");
    // Close file
    fclose(fp);
    if (argc == 3) fclose(fpout);
}

void uncomment(char *s)
{
    for (int i = 0; i < strlen(s); i++)
    {
        if (s[i] == '/' && s[i+1] == '/')
        {
            s[i] = '\n';
            s[i+1] = '\0';
        }
    }
}

int include_file(char *s, FILE *write_file)
{
    if (s[0] == '#' && s[1] == 'i' && s[2] == 'n' && s[3] == 'c' && s[4] == 'l' && s[5] == 'u' && s[6] == 'd' && s[7] == 'e' && s[8] == ' ')
    {
        FILE *ir; // include read
        char include_filename[32];
        for (int i = 0; i < strlen(s); i++)
        {
            //printf("%c", s[i+9]);
            if (s[i+9] != '\n' || s[i+9] != '\0')
            {
                include_filename[i] = s[i+9];
            }
        }

        for (int i = 0; i < strlen(include_filename); i++)
        {
            if (include_filename[i] == '\n' || include_filename[i] == '\0')
            {
                include_filename[i] = '\0';
            }
        }
        
        ir = fopen(include_filename, "r");
        char include_line[100];
        while (fgets(include_line, 100, ir) != NULL)
        {
            //fprintf(fpout, "%s", include_line);
            printf("%s", include_line);
            write_to_file(include_line, write_file);
        }
        write_to_file("\n", write_file);
        fclose(ir);
        for (int i = 0; i < strlen(s); i++)
        {
            if (s[i] != '\n')
            {
                s[i] = '\0';
            }
        }
        //s[0] = '\n';
        return 0;
    }
    return 1;
    
}

void write_to_file(char *s, FILE *write_file)
{
    fprintf(write_file, "%s", s);
}