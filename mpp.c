#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void scrub_comments(char *s);
void include_file(char *s);
void write_to_file(char *s);

FILE *fpout;

int main(int argc, char *argv[])
{
    // Check to see if user supplied 2 arguments
    if (argc < 2)
    {
        printf("Please supply an input and optionally an output file\n");
        exit(2);
    }
    
    // Open first argument for reading
    FILE *fp;
    fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    // Open second argument for writing
    
    if (argc == 3)
    {
        fpout = fopen(argv[2], "w");
        if (!fpout)
        {
            printf("Can't open %s for writing\n", argv[2]);
            exit(1);
        }
    }
    
    // Read all lines and print them as we go
    char line[100];
    while(fgets(line, 100, fp) != NULL)
    {
        scrub_comments(line);
        include_file(line);    
        if (argc == 3)
            fprintf(fpout, "%s", line);
        else
            printf("%s", line);
    }
    printf("\n");
    // Close file
    fclose(fp);
    if (argc == 3) fclose(fpout);
}

void scrub_comments(char *s)
{
    for (int i = 0; i < strlen(s); i++)
    {
        if (s[i] == '/' && s[i+1] == '/')
        {
            s[i] = '\n';
            s[i+1] = '\0';
        }
    }
}

void include_file(char *s)
{
    if (s[0] == '#' && s[1] == 'i' && s[2] == 'n' && s[3] == 'c' && s[4] == 'l' && s[5] == 'u' && s[6] == 'd' && s[7] == 'e' && s[8] == ' ')
    {
        char include_filename[32];
        for (int i = 0; i < strlen(s); i++)
        {
            //printf("%c", s[i+9]);
            if (s[i+9] != '\n' || s[i+9] != '\0')
            {
                include_filename[i] = s[i+9];
            }
        }
        //printf("%s", include_filename);
        FILE *ir; // include read
        for (int i = 0; i < strlen(include_filename); i++)
        {
            if (include_filename[i] == '\n' || include_filename[i] == '\0')
            {
                include_filename[i] = '\0';
            }
        }
        ir = fopen(include_filename, "r");
        char include_line[100];
        while (fgets(include_line, 100, ir) != NULL)
        {
            fprintf(fpout, "%s", include_line);
            printf("%s", include_line);
        }
        fclose(ir);
    }
}